# Practica 3 #

Proyecto hecho con Maven que contiene una libreria para manejar grafos dirigidos junto a un algoritmo que encuentra el camino de un nodo a otro.

Ya que no hay clase principal no se puede compilar normalmente solo, pero al estar hecho con Maven, incluye un fichero de testeo para verificar el funcionamiento:

se puede probar con el comando "mvn test" en consola



Gonzalo Valdez
