/*
 * Copyright 2021 Gonzalo Valdez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.practica3;

import java.util.*;

public class Graph<V> {
    private Map<V, Set<V>> adjacencyList = new HashMap<>();
    
    
    public boolean addVertex(V v){
       if (v == null || containsVertex(v)) return false;
       Set<V> set = new HashSet<>();
       adjacencyList.put(v, set);
       return true;
    }

    public boolean addEdge(V v1, V v2){
        if(!containsVertex(v1)) addVertex(v1);
        if(!containsVertex(v2)) addVertex(v2);

        Set set1 = adjacencyList.get(v1);       
        if (set1.contains(v2)) return false;
        set1.add(v2);
  
        return true;
    }

    public Set<V> obtainAdjacents(V v) throws Exception{
        if(!containsVertex(v)) throw new Exception("No existe el vertice " + v);
        return adjacencyList.get(v);
    }
    
    public boolean containsVertex(V v){
       return adjacencyList.containsKey(v);
    }
    
    @Override
    public String toString(){
       StringBuilder sb = new StringBuilder(); // 
      
        for (V key : adjacencyList.keySet()) {
           sb.append(key);
        }
        return sb.toString();
    }

    public List<V> onePath(V v1, V v2){
        Map<V,V> traza = new HashMap<>();
        Stack<V> abierta = new Stack<V>();
        abierta.push(v1);
        traza.put(v1,null);
        boolean encontrado = false;
        while (!abierta.empty() && !encontrado){
            V v = abierta.pop();
            if(v==v2) encontrado = true;
            if(!encontrado){
                for(V s : adjacencyList.get(v)){
                    abierta.push(s);
                    traza.put(s, v);
                }
            }
        }
        if(encontrado){ //reconstruimos el camino hacia atras
            List<V> path = new ArrayList<>();
            V x = v2;
            while(x != v1){
                path.add(0, x);
                x = traza.get(x);  
            }
            if(x==v1) {
                path.add(0,v1);
                return path;
            };
            
        } 
        
        return null;
    }

}
