/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.practica3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gvald
 */
public class GraphTest {
    
    @Test
    public void testAddVertexNull() {
        Graph g = new Graph();
        boolean result = g.addVertex(null);
        assertEquals(false, result);
    }
    
    @Test
    public void testAddVertex() {
        Graph g = new Graph();
        boolean result = g.addVertex(1);
        assertEquals(true, result);
    }
    
    @Test
    public void testAddVertexExistente() {
        Graph g = new Graph();
        g.addVertex(1);
        boolean result = g.addVertex(1);
        assertEquals(false, result);
    }
    
    @Test (expected = Exception.class)
    public void testobtainAdjacentsNoVertex() throws Exception {
        Graph g = new Graph();
        g.obtainAdjacents(2);     
    }
    
    @Test
    public void testcontainsVertex() {
        Graph g = new Graph();
        g.addVertex(1);
        boolean result = g.containsVertex(1);
        assertEquals(true, result);
    }
    
    @Test
    public void testAddEdge1() {
        Graph g = new Graph();
        g.addEdge(1,2);
        boolean result = g.addEdge(1,2);
        assertEquals(false, result);
    }
    
    @Test
    public void testAddEdge2() {
        Graph g = new Graph();
        g.addVertex(1);
        boolean result = g.addEdge(1,2);
        assertEquals(true, result);
    }
    
    @Test
    public void testtoString() {
        Graph g = new Graph();
        g.addVertex(1);
        g.addEdge(3,4);
        System.out.println(g.toString());
    }
    /**
    * Este test comprueba que el método ‘onePath(V v1, V v2)‘
    * encuentra un camino entre ‘v1‘ y ‘v2‘ cuando existe.
    */
    @Test
    public void onePathFindsAPath(){
        System.out.println("\nTest onePathFindsAPath");
        System.out.println("----------------------");
        // Se construye el grafo.
        Graph<Integer> g = new Graph<>();
        g.addEdge(1, 2);
        g.addEdge(3, 4);
        g.addEdge(1, 5);
        g.addEdge(5, 6);
        g.addEdge(6, 4);
        // Se construye el camino esperado.
        List<Integer> expectedPath = new ArrayList<>();
        expectedPath.add(1);
        expectedPath.add(5);
        expectedPath.add(6);
        expectedPath.add(4);
        //Se comprueba si el camino devuelto es igual al esperado.
        assertEquals(expectedPath, g.onePath(1, 4));
    }

}
